<?php
    require 'src/utils/utils.php';
    require 'src/api/beer_mapping_api.php';
    require 'src/api/geolocation_api.php';
    session_start(); // calling session_start() to resume the existing session
?>
<!doctype html>
<html lang="en">
<?php require_once 'templates/header.php'; ?>
<body>
<?php require_once 'templates/navigation.php'; ?>

<main class="container">
    <div class="row col-12">
        <h1 class="title">View Brewery</h1>
    </div>
    <?php
        // setting all necessary parameters to load the page
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $lat = isset($_GET['lat']) ? $_GET['lat'] : null;
        $lon = isset($_GET['lng']) ? $_GET['lng'] : null;
        $ipAddress = get_geolocation_by_ip_address($_SESSION['IP_ADDRESS']);
        // retrieve brewery based on the user's selection
        $images = get_brewery_image_by_id($id);
        $brewery = find_brewery_by_piece($id);
        if (!empty($brewery)) {
            ?>
            <div class="row col-12">
                <div class="col-6" id="map"></div>
                <div class="col-6">
                    <table class="table table-dark">
                        <tbody>
                        <tr>
                            <th scope="row">Name:</th>
                            <td><?php echo $brewery[0]['name'] ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Address:</th>
                            <td><?php echo trim(sprintf("%s, %s, %s %s", $brewery[0]['street'], $brewery[0]['city'], $brewery[0]['state'], $brewery[0]['zip'])) ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Phone Number:</th>
                            <td><?php echo !empty($brewery[0]['phone']) ? $brewery[0]['phone'] : "N/A" ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Website:</th>
                            <td>
                                <?php
                                    // if links is valid display the link, if not display N/A
                                    if (!empty($brewery[0]['url'])) {
                                        ?>
                                        <a href="<?php echo format_brewery_url(rawurldecode($brewery[0]['url'])) ?>"><?php echo format_brewery_url(rawurldecode($brewery[0]['url'])) ?></a>
                                        <?php
                                    } else {
                                        echo "N/A";
                                    }
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Reviews:</th>
                            <td>
                                <?php
                                    if (!empty($brewery[0]['reviewlink'])) {
                                        ?>
                                        <a href="<?php echo format_brewery_url(rawurldecode($brewery[0]['reviewlink'])) ?>"><?php echo format_brewery_url(rawurldecode($brewery[0]['reviewlink'])) ?></a>
                                        <?php
                                    } else {
                                        echo "N/A";
                                    }
                                ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <?php
            // display images if there are images to display
            if (!empty($images)) {
                ?>
                <div class="row col-12 justify-content-center">
                    <h2 id="location_images">Location Images</h2>
                </div>
                <div class="row col-12 justify-content-center">
                    <div id="demo" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ul class="carousel-indicators">
                            <?php
                                // use bootstrap 4 carousel to display the images
                                for ($i = 0; $i < sizeof($images); $i++) {
                                    // check if index exists
                                    if (array_key_exists($i, $images)) {
                                        if ($i == 0) {
                                            ?>
                                            <li data-target="#demo" data-slide-to="<?php echo $i; ?>"
                                                class="active"></li>
                                            <?php
                                        } else {
                                            ?>
                                            <li data-target="#demo" data-slide-to="<?php echo $i; ?>"></li>
                                            <?php
                                        }
                                    }
                                }
                            ?>
                        </ul>
                        <!-- The slideshow -->
                        <div class="carousel-inner">
                            <?php
                                // use bootstrap 4 carousel to display the images
                                for ($i = 0; $i < sizeof($images); $i++) {
                                    // check if index exists
                                    if (array_key_exists($i, $images)) {
                                        if ($i == 0) {
                                            ?>
                                            <div class="carousel-item active">
                                                <img src="<?php echo $images[$i]['imageurl']; ?>"
                                                     alt="<?php echo $images[$i]['caption']; ?>">
                                            </div>
                                            <?php
                                        } else {
                                            ?>
                                            <div class="carousel-item">
                                                <img src="<?php echo $images[$i]['imageurl']; ?>"
                                                     alt="<?php echo $images[$i]['caption']; ?>">
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                            ?>
                        </div>
                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                    </div>
                </div>
                <?php
            }
        } else {
            ?>
            <div class="alert alert-danger" role="alert">
                Failed to retrieve brewery please go back to the search page and try again...
            </div>
            <?php
        }
    ?>
</main>

<!-- dynamically build the leaflet map from the values found from the API
     only proceed if the latitude, longitude, and brewery are valid -->
<?php if (!empty($lat) && !empty($lon) && !empty($brewery)) { ?>
    <script>
        // setting the primary view of brewery
        let map = L.map('map').setView([<?php echo $lat . ', ' . $lon?>], 13);

        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
            maxZoom: 18,
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
                '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
                'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1
        }).addTo(map);

        // brewery location
        L.marker([<?php echo $lat . ', ' . $lon?>]).addTo(map).bindPopup("<?php echo $brewery[0]['name'] . ', ' . $brewery[0]['city'] . ', ' . $brewery[0]['state'] ?>").openPopup();
        <?php if (!empty($ipAddress)) {
        ?>
        // location of user
        L.marker([<?php echo $ipAddress['latitude'] . ', ' . $ipAddress['longitude']?>]).addTo(map).bindPopup("You are here...").openPopup();
        <?php
        }
        ?>
    </script>
<?php } ?>

<?php require_once 'templates/scripts.php'; ?>
</body>
</html>