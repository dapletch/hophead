<?php
    require 'src/search_breweries.php';

    $results = [];

    if (!isset($_REQUEST['term'])) {
        echo json_encode($results);
        return;
    }

    // add successful response code and output JSON
    http_response_code(200);
    echo json_encode(
        autocomplete_breweries(strtolower(trim($_REQUEST['term']))),
        JSON_PRETTY_PRINT
    );
