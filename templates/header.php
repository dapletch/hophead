<?php include_once 'src/utils/utils.php' ?>
<head>
    <!-- Including  -->
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS, primarily for the purposes of utilizing CSS grid -->
    <link rel="stylesheet" href="<?php echo my_server_url(); ?>/hophead/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo my_server_url(); ?>/hophead/css/ui/1.12.1/jquery-ui.min.css">
    <link rel="stylesheet" href="<?php echo my_server_url(); ?>/hophead/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo my_server_url(); ?>/hophead/js/leaflet/leaflet.css">
    <script src="<?php echo my_server_url(); ?>/hophead/js/leaflet/leaflet.js"></script>
    <link rel="stylesheet" href="<?php echo my_server_url(); ?>/hophead/css/style.css">
    <title>Hophead Brewery Finder</title>
    <!-- Page title should be no longer than 65 characters according to the reading -->
    <meta name="title" content="Hophead: Find a brew near you..."/>
    <meta name="description"
          content=""/>
    <!-- Web crawlers prefer no spaces in keywords -->
    <meta name="keywords"
          content=""/>
    <meta property="og:title"
          content=""/>
    <meta property="og:type" content="website"/>
    <meta property="og:image" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:description"
          content=""/>
    <meta name="robots" content="index,nofollow">
</head>