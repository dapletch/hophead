<?php include_once 'src/utils/utils.php' ?>
<!-- Importing the JS files at the bottom of the page  -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo my_server_url(); ?>/hophead/js/jquery-3.3.1.min.js"></script>
<script src="<?php echo my_server_url(); ?>/hophead/js/ui/1.12.1/jquery-ui.min.js"></script>
<script src="<?php echo my_server_url(); ?>/hophead/js/popper.min.js"></script>
<script src="<?php echo my_server_url(); ?>/hophead/js/bootstrap.min.js"></script>

<script type="application/javascript">
    $(function () {
        $('#search_criteria').autocomplete({
            source: "autocomplete.php",
            minLength: 5
        });
    });

    $(document).ready(function() {
        // setting the ip_address form field when available
        $.get("http://api.ipify.org/?format=json", function (data) {
            // only proceed when form element exists on page
            let ipAddress = $('input[id=ip_address]');
            if (ipAddress.length) {
                ipAddress.val(data.ip)
            }
        });
    });

</script>