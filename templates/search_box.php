<div class="row col-12 justify-content-end">
    <h1 class="title">Hophead</h1>
</div>
<form action="<?php echo my_server_url() ?>/hophead/search.php" method="post" id="brew_search">
    <div class="row col-12">
        <div class="input-group mb-3">
            <input type="text" class="form-control" id="search_criteria" name="search_criteria"
                   placeholder="Search either by zip code or brewery name"
                   aria-label="Search Beer" aria-describedby="Search Input">
            <div class="input-group-append">
                <button class="btn btn-outline-primary" type="submit">Search</button>
            </div>
        </div>
        <input type="hidden" id="ip_address" name="ip_address" value="">
    </div>
</form>
