<?php
    require 'src/search_breweries.php';
    require 'src/api/geolocation_api.php';
    // starting session to save the variable
    session_start();
?>

<!doctype html>
<html lang="en">
<?php require_once 'templates/header.php'; ?>
<body>
<?php require_once 'templates/navigation.php'; ?>

<main class="container">
    <?php
        // importing the search box to enable the user to refine their search
        require_once 'templates/search_box.php';
        // setting user ip address into session to be used on other pages
        $_SESSION['IP_ADDRESS'] = isset($_POST['ip_address']) ? $_POST['ip_address'] : null;
        if (isset($_POST['search_criteria'])) {
            // retrieve the results based on the user input
            $results = get_breweries(strtolower(trim($_REQUEST['search_criteria'])));
            if (!empty($results)) {
                ?>
                <div class="row col-12">
                    <table class="table table-dark">
                        <thead>
                        <tr>
                            <th scope="col">Name</th>
                            <th scope="col">Address</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Website</th>
                            <th scope="col">View</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            for ($i = 0; $i < sizeof($results); $i++) {
                                if (array_key_exists($i, $results)) {
                                    echo "<tr>";
                                    echo "<td>" . $results[$i]['name'] . "</td>";
                                    echo "<td>" . sprintf("%s, %s, %s %s", $results[$i]['street'], $results[$i]['city'], $results[$i]['state'], $results[$i]['zip']) . "</td>";
                                    echo "<td>" . $results[$i]['phone'] . "</td>";
                                    $url = format_brewery_url(rawurldecode($results[$i]['url']));
                                    echo "<td>" . sprintf('<a href="%s">%s</a>', $url, $url)
                                        . "</td>";
                                    // construct the link to populate button so that a specific brewery can be loaded
                                    echo "<td>" . sprintf(
                                            '<a href="%s" class="btn btn-primary">View Brewery</a>',
                                            ('view_brewery.php?' . http_build_query(['id' => $results[$i]['id'], 'lat' => $results[$i]['lat'], 'lng' => $results[$i]['lng']])))
                                        . "</td>";
                                    echo "</tr>";
                                }
                            }
                        ?>
                        </tbody>
                    </table>
                </div>
                <?php
            } else {
                ?>
                <div class="alert alert-dark" role="alert">
                    No results found for the search term entered
                </div>
                <?php
            }
            ?>
            <?php
        } else {
            ?>
            <div class="alert alert-dark" role="alert">
                Please enter a valid search term.
            </div>
            <?php
        }
    ?>
</main>

<?php require_once 'templates/scripts.php'; ?>
</body>
</html>