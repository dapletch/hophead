<!doctype html>
<html lang="en">
<?php require_once 'templates/header.php'; ?>
<body>
<?php require_once 'templates/navigation.php'; ?>

<!-- This page serves as the main landing page for the hophead application -->
<main class="container">
    <?php require_once 'templates/search_box.php'?>
    <!-- TODO add some content to provide context here -->
</main>

<?php require_once 'templates/scripts.php'; ?>
</body>
</html>