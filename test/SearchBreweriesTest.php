<?php
    include '../src/search_breweries.php';

    class SearchBreweriesTest extends PHPUnit_Framework_TestCase
    {
        protected function setUp()
        {
            // to allow for usage of the config.php file
            // https://stackoverflow.com/questions/15193816/phpunit-doesnt-allow-me-to-include-files
            set_include_path("C:/wamp64/www/hophead/");
        }

        public function test_get_breweries() {
            $breweries = get_breweries("Vermont Pub and Brewery");
            print_r($breweries);
            self::assertTrue(!empty($breweries));
        }
    }
