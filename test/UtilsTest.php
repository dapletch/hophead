<?php
include '../src/utils.php';

class UtilsTest extends PHPUnit_Framework_TestCase
{

    protected function setUp()
    {
        $_SERVER['SERVER_NAME'] = 'localhost';
        // we're only testing on http, so okay
        $_SERVER['HTTPS'] = null;
    }

    public function test_my_server_url()
    {
        $url = my_server_url();
        print_r($url);
        self::assertTrue(isset($url));
    }

    public function test_is_zip_code()
    {
        self::assertTrue(is_zip_code("12345"));
    }

}
