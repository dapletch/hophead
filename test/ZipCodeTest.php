<?php
include '../src/zip_code_api.php';

class ZipCodeTest extends PHPUnit_Framework_TestCase
{

    // find_place_state_by_zip_code
    public function test_find_place_state_by_zip_code()
    {
        $location = find_state_by_zip_code("05301");
        print_r($location);
        self::assertTrue(!empty($location));
    }

    public function test_find_place_state_by_zip_code_no_results()
    {
        // obviously invalid input
        $location = find_state_by_zip_code("qwerty");
        print_r($location);
        self::assertFalse(!empty($location));
    }

}
