<?php


class AutocompleteTest extends PHPUnit_Framework_TestCase
{
    // Note: this unit test can only be ran while the application is up and running
    public function test_autocomplete()
    {
        self::assertTrue(!empty(json_decode(file_get_contents("http://localhost/hophead/autocomplete.php?search_criteria=05301"))));
    }

    // Note: this unit test can only be ran while the application is up and running
    public function test_autocomplete_partial_zip_code()
    {
        self::assertFalse(!empty(json_decode(file_get_contents("http://localhost/hophead/autocomplete.php?search_criteria=053"))));
    }

}
