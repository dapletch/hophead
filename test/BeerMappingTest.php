<?php
include '../src/beer_mapping_api.php';

class BeerMappingTest extends PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        // to allow for usage of the config.php file
        // https://stackoverflow.com/questions/15193816/phpunit-doesnt-allow-me-to-include-files
        set_include_path("C:\wamp64\www\hophead\\");
    }

    public function test_find_brewery_by_name()
    {
        $breweries = find_brewery_by_piece("Harpoon");
        print_r($breweries);
        self::assertTrue(!empty($breweries));
    }

    public function test_find_brewery_by_id()
    {
        $breweries = find_brewery_by_piece("19084");
        print_r($breweries);
        self::assertTrue(!empty($breweries));
    }

    // find_breweries_by_city
    public function test_find_breweries_by_city()
    {
        $breweries = find_breweries_by_city("Brattleboro", "VT");
        print_r($breweries);
        self::assertTrue(!empty($breweries));
    }

    // used to determine when the query parameters don't return anything
    public function test_is_brewery_result_valid()
    {
        self::assertFalse(is_brewery_result_valid(find_brewery_by_piece("Bur")));
    }

    // get_brewery_map_by_id
    public function test_get_brewery_map_by_id()
    {
        $map = get_brewery_map_by_id("838");
        print_r($map);
        self::assertFalse(!empty($map));
    }

    // get_brewery_image_by_id
    public function test_get_brewery_image_by_id()
    {
        $image = get_brewery_image_by_id("838");
        print_r($image);
        self::assertTrue(!empty($image));
    }

    // no need to check for null when you can use empty
    public function test_is_null_empty()
    {
        self::assertTrue(empty(null));
    }

    public function test_is_brewery_map_valid()
    {
        self::assertFalse(is_brewery_map_valid(get_brewery_map_by_id("838")));
    }

    public function test_is_brewery_image_valid()
    {
        self::assertFalse(is_brewery_image_valid(get_brewery_image_by_id("838")));
    }

}
