<?php
include '../src/geolocation_api.php';

class GeolocationTest extends PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        // to allow for usage of the config.php file
        // https://stackoverflow.com/questions/15193816/phpunit-doesnt-allow-me-to-include-files
        set_include_path("C:\wamp64\www\hophead\\");
    }

    public function test_get_geolocation_by_ip_address()
    {
        $location = get_geolocation_by_ip_address('72.2.178.49');
        print_r($location);
        self::assertTrue(!empty($location));
    }
}
