<?php
// resolving relative paths on Windows: https://yagudaev.com/posts/resolving-php-relative-path-problem/
    require 'src/utils/utils.php';
    require 'src/api/zip_code_api.php';
    require 'src/api/nominatim_api.php';
    require 'src/api/beer_mapping_api.php';

    /**
     * Query for the breweries based on the user input and return the brewery information along
     * with the geolocation data i.e. latitude and longitude
     *
     * @param $query
     * @return array|mixed
     */
    function get_breweries($query)
    {
        $breweries = [];
        if (isset($query)) {
            try {
                $results = [];
                $latitude = '';
                $longitude = '';
                // check to see if the user input is a zip code
                // if so proceed to get the state abbreviation along with the town to pass to
                if (is_zip_code($query)) {
                    // retrieve the state abbreviation and town from the location
                    $location = find_state_by_zip_code($query);
                    if (!empty($location)) {
                        $results = find_breweries_by_city(
                            $location['places'][0]['place name'],
                            $location['places'][0]['state abbreviation']
                        );
                    }
                } else {
                    // if the input is not a zip code then search by the brewery name
                    $results = find_brewery_by_piece($query);
                }
                // make sure results is not empty, the proceed
                if (!empty($results)) {
                    // now query for the geolocation of each brewery and append it to 'View Brewery'
                    for ($i = 0; $i < sizeof($results); $i++) {
                        $map = get_brewery_map_by_id($results[$i]['id']);
                        if (!empty($map)) {
                            $latitude = $map[0]['lat'];
                            $longitude = $map[0]['lng'];
                        } else {
                            // data was not found via the brewery map api, defaulting to the nominatim api
                            $map = search_by_address($results[$i]['street'], $results[$i]['city'], $results[$i]['state'], $results[$i]['country'], $results[$i]['zip']);
                            if (!empty($map)) {
                                $latitude = $map[0]['lat'];
                                $longitude = $map[0]['lon'];
                            } else {
                                // now try again, except this time split the name by a comma to make street less stringent
                                // i.e. '29 High St., Ste 101C' becomes '29 High St.'
                                if (strpos($results[$i]['street'], ',')) {
                                    // split the string to get revised search criteria for street
                                    $street = explode(',', $results[$i]['street']);
                                    $map = search_by_address($street[0], $results[$i]['city'], $results[$i]['state'], $results[$i]['country'], $results[$i]['zip']);
                                    if (!empty($map)) {
                                        $latitude = $map[0]['lat'];
                                        $longitude = $map[0]['lon'];
                                    }
                                }
                            }
                        }
                        // append results the array
                        array_push(
                            $breweries, [
                                'lat' => $latitude,
                                'lng' => $longitude,
                                'id' => $results[$i]['id'],
                                'name' => $results[$i]['name'],
                                'street' => $results[$i]['street'],
                                'city' => $results[$i]['city'],
                                'state' => $results[$i]['state'],
                                'zip' => $results[$i]['zip'],
                                'phone' => $results[$i]['phone'],
                                'url' => format_brewery_url($results[$i]['url'])
                            ]
                        );
                    }
                }
            } catch (Exception $e) {
                error_log($e->getMessage());
            }
        }
        return $breweries;
    }

    /**
     * Returns filtered array for autocomplete functionality within JQuery
     *
     * @param $query
     * @return array
     */
    function autocomplete_breweries($query)
    {
        $breweries = [];
        if (isset($query)) {
            $results = [];
            if (is_zip_code($query)) {
                // retrieve the state abbreviation and town from the location
                $location = find_state_by_zip_code($query);
                if (!empty($location)) {
                    $results = find_breweries_by_city(
                        $location['places'][0]['place name'],
                        $location['places'][0]['state abbreviation']
                    );
                }
            } else {
                // if the input is not a zip code then search by the brewery name
                $results = find_brewery_by_piece($query);
            }
            // validate results and then filter JSON for autocomplete functionality in JQuery
            if (!empty($results)) {
                for ($i = 0; $i < sizeof($results); $i++) {
                    array_push($breweries,
                        ['label' => $results[$i]['name'], 'value' => $results[$i]['name']]
                    );
                }
            }
        }
        return $breweries;
    }