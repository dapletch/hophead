<?php
/**
 * Returns the url of the server for pulling in assets into templates
 * @return string
 */
function my_server_url()
{
    $server_name = $_SERVER['SERVER_NAME'];

    if (!empty($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) == 'on' || $_SERVER['HTTPS'] == '1')) {
        $scheme = 'https';
    } else {
        $scheme = 'http';
    }
    return $scheme . '://' . $server_name;
}

/**
 * @param $zipCode
 * @return bool
 */
function is_zip_code($zipCode)
{
    return (preg_match('#[0-9]{5}#', $zipCode)) ? true : false;
}

/**
 * Return the IP
 *
 * @return mixed
 */
function remote_ip_address()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        //ip from share internet
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        //ip pass from proxy
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}