<?php
require_once 'src/utils/utils.php';
require_once 'src/constants/ZipCodeConstants.php';
/**
 * Returns the town and state information for a given zip code
 * @param $zipCode
 * @return array
 */
function find_state_by_zip_code($zipCode)
{
    $results = [];
    if (isset($zipCode)) {
        if (is_zip_code($zipCode)) {
            try {
                // hardcoding nation code, we're only concerned about the us
                $endpoint = sprintf(ZipCodeConstants::ZIP_CODE, 'us', $zipCode);
                $connection = curl_init();

                curl_setopt($connection, CURLOPT_URL, $endpoint);
                curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);

                $location = curl_exec($connection);
                $err = curl_error($connection);

                curl_close($connection);

                if ($err) {
                    error_log("cURL Error #:" . $err);
                } else {
                    // check for empty response
                    if (!empty($location)) {
                        $results = json_decode($location, JSON_OBJECT_AS_ARRAY);
                    }
                }
            } catch (Exception $e) {
                error_log($e->getMessage());
            }
        }
    }
    return $results;
}
