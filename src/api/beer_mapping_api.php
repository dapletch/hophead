<?php
    require_once 'src/constants/BeerMappingConstants.php';

    /**
     * This query can be made either with a location name or with a location id.
     *
     * Search by URL Encoded Brewery name
     * http://beermapping.com/webservice/locquery/7b86a9807f95c57de6ae1098616d04b2/burlington%20beer%20company&s=json
     *
     * OR by the Brewery ID
     * http://beermapping.com/webservice/locquery/7b86a9807f95c57de6ae1098616d04b2/19084&s=json
     *
     * @param $piece
     * @return array
     */
    function find_brewery_by_piece($piece)
    {
        $results = [];
        if (isset($piece)) {
            try {
                // checking to see if the configuration is complete
                $config = include('conf/config.php');
                // check to see if any values were loaded
                if (!empty($config)) {
                    // api endpoint all nice and formatted
                    $endpoint = sprintf(BeerMappingConstants::LOCQUERY, $config['beermapping.api.key'], rawurlencode(strtolower($piece)));
                    $connection = curl_init();

                    curl_setopt($connection, CURLOPT_URL, $endpoint);
                    curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);

                    $breweries = curl_exec($connection);
                    $err = curl_error($connection);

                    curl_close($connection);

                    if ($err) {
                        error_log("cURL Error #:" . $err);
                    } else {
                        // check for valid brewery result
                        $results = is_brewery_result_valid(json_decode($breweries, JSON_OBJECT_AS_ARRAY)) ?
                            json_decode($breweries, JSON_OBJECT_AS_ARRAY) : $results;
                    }
                }
            } catch (Exception $e) {
                error_log($e->getMessage());
            }
        }
        return $results;
    }

    /**
     * Returns a list of breweries in a given city based depending on the state provided
     *
     * @param $city
     * @param $stateCode
     * @return array|mixed
     */
    function find_breweries_by_city($city, $stateCode)
    {
        $results = [];
        if (isset($city) && isset($stateCode)) {
            try {
                // checking to see if the configuration is complete
                $config = include('conf/config.php');
                // check to see if any values were loaded
                if (!empty($config)) {
                    // encode the params and load them into the url
                    // parameters being the city and the stateCode
                    $endpoint = sprintf(
                        BeerMappingConstants::LOCCITY,
                        $config['beermapping.api.key'],
                        rawurlencode(strtolower($city)),
                        rawurlencode(strtolower($stateCode))
                    );
                    $connection = curl_init();

                    curl_setopt($connection, CURLOPT_URL, $endpoint);
                    curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);

                    $breweries = curl_exec($connection);
                    $err = curl_error($connection);

                    curl_close($connection);

                    if ($err) {
                        error_log("cURL Error #:" . $err);
                    } else {
                        // check for valid brewery result
                        $results = is_brewery_result_valid(json_decode($breweries, JSON_OBJECT_AS_ARRAY)) ?
                            json_decode($breweries, JSON_OBJECT_AS_ARRAY) : $results;
                    }
                }
            } catch (Exception $e) {
                error_log($e->getMessage());
            }
        }
        return $results;
    }

    /**
     * Determine is if the result returned from either the LOCQUERY or LOCCITY endpoints is valid
     *
     * @param $breweries
     * @return bool
     */
    function is_brewery_result_valid($breweries)
    {
        if (!empty($breweries)) {
            return !($breweries[0]['id'] == 0 && ($breweries[0]['name'] == 'Try a Longer Search' || $breweries[0]['name'] == 'No locations Found'));
        }
        return false;
    }

    /**
     * Determine if the brewery URL is valid and if not reformat it so it is
     *
     * @param $url
     * @return mixed
     */
    function format_brewery_url($url)
    {
        if (!empty($url)) {
            if (!filter_var($url, FILTER_VALIDATE_URL)) {
                // defaulting to http for it's a standard protocol, most sites will redirect to to the https if valid
                $url = (!strpos($url, "www") ? sprintf("http://www.%s", $url) : $url);
            }
        }
        return $url;
    }

    /**
     * Returns the brewery map information i.e. latitude and longitude to plugin into the
     *
     * @param $id
     * @return array
     */
    function get_brewery_map_by_id($id)
    {
        $results = [];
        if (isset($id)) {
            try {
                // checking to see if the configuration is complete
                $config = include('conf/config.php');
                // check to see if any values were loaded
                if (!empty($config)) {
                    $endpoint = sprintf(BeerMappingConstants::LOCMAP, $config['beermapping.api.key'], $id);
                    $connection = curl_init();

                    curl_setopt($connection, CURLOPT_URL, $endpoint);
                    curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);

                    $map = curl_exec($connection);
                    $err = curl_error($connection);

                    curl_close($connection);

                    if ($err) {
                        error_log("cURL Error #:" . $err);
                    } else {
                        // check for empty response
                        $results = is_brewery_map_valid(json_decode($map, JSON_OBJECT_AS_ARRAY)) ?
                            json_decode($map, JSON_OBJECT_AS_ARRAY) : $results;
                    }
                }
            } catch (Exception $e) {
                error_log($e->getMessage());
            }
        }
        return $results;
    }

    /**
     * Determines if the Brewery Array returned is valid, and can be subsequently used to obtain the latitude and longitude
     *
     * @param $map
     * @return bool
     */
    function is_brewery_map_valid($map)
    {
        $result = false;
        if (!empty($map)) {
            // should use a for loop since we're analyzing an array
            // and a brewery can theoretically have more than once location
            for ($i = 0; $i <= sizeof($map); $i++) {
                // determine if index exists or not, and then proceed
                if (array_key_exists($i, $map)) {
                    foreach (array_keys($map[$i]) as $key) {
                        if (!empty($map[$i][$key])) {
                            if ($key == 'lat' || $key == 'lng') {
                                $result = $map[$i][$key] != '0.000000';
                            }
                        }
                    }
                }
            }
        }
        return $result;
    }

    /**
     * Get the brewery image links via the ID
     *
     * @param $id
     * @return array
     */
    function get_brewery_image_by_id($id)
    {
        $results = [];
        if (isset($id)) {
            try {
                // checking to see if the configuration is complete
                $config = include('conf/config.php');
                // check to see if any values were loaded
                if (!empty($config)) {
                    $endpoint = sprintf(BeerMappingConstants::LOCIMAGE, $config['beermapping.api.key'], $id);
                    $connection = curl_init();

                    curl_setopt($connection, CURLOPT_URL, $endpoint);
                    curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);

                    $image = curl_exec($connection);
                    $err = curl_error($connection);

                    curl_close($connection);

                    if ($err) {
                        error_log("cURL Error #:" . $err);
                    } else {
                        // check for empty response
                        if (!empty($image)) {
                            // only assign the result if the the image is valid
                            $results = is_brewery_image_valid(json_decode($image, JSON_OBJECT_AS_ARRAY)) ?
                                json_decode($image, JSON_OBJECT_AS_ARRAY) : $results;
                        }
                    }
                }
            } catch (Exception $e) {
                error_log($e->getMessage());
            }
        }
        return $results;
    }

    /**
     * Determines if there are any valid values returned from the Brewery Image Endpoint that are not null
     *
     * @param $image
     * @return bool
     */
    function is_brewery_image_valid($image)
    {
        $result = false;
        if (!empty($image)) {
            for ($i = 0; $i <= sizeof($image); $i++) {
                // determine if index exists or not, and then proceed
                if (array_key_exists($i, $image)) {
                    foreach (array_keys($image[$i]) as $key) {
                        if (!empty($image[$i][$key])) {
                            $result = true;
                            // at least one value within the array
                            // has to be not null to return true
                            break;
                        }
                    }
                }
            }
        }
        return $result;
    }
