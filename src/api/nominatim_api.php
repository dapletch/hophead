<?php
    require_once 'src/constants/NominatimConstants.php';

    /**
     * Returns the geolocation data from the Nominatim API
     *
     * [
     * {
     * "place_id": 133261895,
     * "licence": "Data © OpenStreetMap contributors, ODbL 1.0. https://osm.org/copyright",
     * "osm_type": "way",
     * "osm_id": 201649400,
     * "boundingbox": [
     * "44.4565942",
     * "44.4571711",
     * "-73.2211429",
     * "-73.2202666"
     * ],
     * "lat": "44.45680085",
     * "lon": "-73.22068075062273",
     * "display_name": "160, Flynn Avenue, South end, Burlington, Chittenden County, Vermont, 05401, United States of America",
     * "class": "building",
     * "type": "yes",
     * "importance": 0.42099999999999993
     * }
     * ]
     *
     * CURLOPT_USERAGENT Required otherwise the following error will occur:
     * <html>
     * <head>
     * <title>Access blocked</title>
     * </head>
     * <body>
     * <h1>Access blocked</h1>
     *
     * <p>You have been blocked because you have violated the
     * <a href="https://operations.osmfoundation.org/policies/nominatim/">usage policy</a>
     * of OSM's Nominatim geocoding service. Please be aware that OSM's resources are
     * limited and shared between many users. The usage policy is there to ensure that
     * the service remains usable for everybody.</p>
     *
     * <p>Please review the terms and make sure that your
     * software adheres to the terms. You should in particular verify that you have set a
     * <b>custom HTTP referrer or HTTP user agent</b> that identifies your application, and
     * that you are not overusing the service with massive bulk requests.</p>
     *
     * <p>If you feel that this block is unjustified or remains after you have adopted
     * your usage, you may contact the Nominatim system administrator at
     * nominatim@openstreetmap.org to have this block lifted.</p>
     * </body>
     * </head>
     *
     * @param $street
     * @param $city
     * @param $state
     * @param $country
     * @param $postalCode
     * @return array
     */
    function search_by_address($street, $city, $state, $country, $postalCode)
    {
        $results = [];
        if (isset($street) && isset($city) &&
            isset($state) && isset($country) && isset($postalCode)) {
            try {

                // build url encoded version of endpoints
                $endpoint = sprintf(
                    NominatimConstants::SEARCH,
                    rawurlencode($street),
                    rawurlencode($city),
                    rawurlencode($state),
                    rawurlencode($country),
                    rawurlencode($postalCode)
                );

                $connection = curl_init();

                curl_setopt($connection, CURLOPT_URL, $endpoint);
                curl_setopt($connection, CURLOPT_RETURNTRANSFER, true);
                // shouldn't ignore the SLL Certificate, but since not going to a production environment...OK
                curl_setopt($connection, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($connection, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($connection, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

                $map = curl_exec($connection);
                $err = curl_error($connection);

                curl_close($connection);

                if ($err) {
                    error_log("cURL Error #:" . $err);
                } else {
                    // check for empty response
                    $results = json_decode($map, JSON_OBJECT_AS_ARRAY);
                }

            } catch (Exception $e) {
                error_log($e->getMessage());
            }
        }
        return $results;
    }
