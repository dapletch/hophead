<?php
require_once 'src/constants/GeolocationConstants.php';

function get_geolocation_by_ip_address($ipAddress)
{
    $results = [];
    if (isset($ipAddress)) {
        try {
            // setting up curl to obtain geolocation data of ip address
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => sprintf(GeolocationConstants::IP_ADDRESS, $ipAddress),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                // shouldn't ignore the SLL Certificate, but since not going to a production environment...OK
                CURLOPT_SSL_VERIFYHOST => false,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "accept: application/json",
                    "content-type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                error_log("cURL Error #:" . $err);
            } else {
                $results = json_decode($response, JSON_OBJECT_AS_ARRAY);
            }
        } catch (Exception $e) {
            error_log($e->getMessage());
        }
    }
    return $results;
}