<?php

    /**
     * API Reference for Geolocation: https://nominatim.org/release-docs/develop/api/Overview/
     * Class NominatimConstants
     */
    class NominatimConstants
    {
        // https://nominatim.org/release-docs/develop/api/Search/
        // example: https://nominatim.openstreetmap.org/search/?street=160%20Flynn%20Avenue&city=Burlington&state=VT&country=us&postalCode=5401&format=json
        const SEARCH = "https://nominatim.openstreetmap.org/search/?street=%s&city=%s&state=%s&country=%s&postalCode=%s&format=json";
    }