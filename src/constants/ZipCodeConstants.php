<?php


    /**
     * Class ZipCodeConstants
     * This class is a reference to the API provided by: https://zippopotam.us/ which enables developers to get the following information:
     * {
     *   "post code":"05404",
     *   "country":"United States",
     *   "country abbreviation":"US",
     *   "places":[
     *              {
     *                  "place name":"Winooski",
     *                  "longitude":"-73.1874",
     *                  "state":"Vermont",
     *                  "state abbreviation":"VT",
     *                  "latitude":"44.4949"
     *              }
     *           ]
     * }
     */
    class ZipCodeConstants
    {
        // two arguments, but we're really interested in the second being the zip code the first is the country code,
        // but we're only concerned with the US so we'll hard code for now in the API calls
        const ZIP_CODE = "http://api.zippopotam.us/%s/%s";
    }