<?php

    /**
     * Class BeerMappingConstants
     * Used as the reference point for the API endpoints for the BeerMapping API
     * https://beermapping.com/api/reference/
     *
     * NOTE: the default return type for API queries is xml. If you want json returned instead,
     * simply add &s=json to the end of any of these queries.
     */
    class BeerMappingConstants
    {
        // use string formatting to populate the API key from the configuration, and finally the name of the brewery
        const LOCQUERY = "http://beermapping.com/webservice/locquery/%s/%s&s=json";

        const LOCCITY = "http://beermapping.com/webservice/loccity/%s/%s,%s&s=json";

        const LOCMAP = "http://beermapping.com/webservice/locmap/%s/%s&s=json";

        const LOCIMAGE = "http://beermapping.com/webservice/locimage/%s/%s&s=json";
    }
