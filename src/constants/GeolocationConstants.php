<?php

/**
 * Free API to use to obtain the Geolocation of a particular IP Address. Will prove to be useful when obtaining the IP Address of the end user.
 *
 * Class GeolocationConstants
 */
class GeolocationConstants
{
    // example: https://freegeoip.app/json/72.2.178.49
    const IP_ADDRESS = "https://freegeoip.app/json/%s";
}